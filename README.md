# Sample project for developing Bun Framework

This is a testing repository for developing Bun Framework. Used for development tutorial on my personal blog http://jakulov.ru

- - -

## 1. Installation

Clone repository to your local machine via `git clone https://bitbucket.org/jakulov/bun.git`

### Configure web server environment

I recommend to use nginx web server. I usually creates file: `/etc/nginx/sites-available/framework`, and configure server:

    upstream phpfcgi {
        server unix:/var/run/php5-fpm.sock;
    }

    server {
            listen 80;
            root /home/yakov/projects/framework_bun/app;
            index index.php;
            server_name framework.dev;

            rewrite ^/index\.php/?(.*)$ /$1 permanent;

            location / {
                    index index.php;
                    try_files $uri @rewriteap;
            }

            location @rewriteap {
                    rewrite  ^(.*)$ /index.php/$1 last;
            }

            location ~ ^/(index)\.php(/|$) {
                    fastcgi_pass phpfcgi;
                    fastcgi_split_path_info ^(.+\.php)(/.*)$;
                    include fastcgi_params;
                    fastcgi_param  SCRIPT_FILENAME $document_root$fastcgi_script_name;
                    fastcgi_param  HTTPS off;
            }
    }

You probably will need to replace some params, such fastcgi_pass, server_name and root directory. Also i added `framework.dev` into my hosts file.

### It's almost done.
Go to your project dir and run `php composer.phar install`. Composer should install dependencies, configured in composer.json file and create autoloader for framework and sample application.
Now you can open in your web browser `http://framework.dev` (or host you've created) to see what's happening.