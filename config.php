<?php
/**
 * Require and configure application
 */

// defining environment
if(file_exists(__DIR__ .'/env.php')) {
    require __DIR__ .'/env.php';
}
if(!defined('ENV')) {
    define('ENV', 'dev');
}