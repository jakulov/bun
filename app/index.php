<?php
require __DIR__ .'/../config.php';
require __DIR__ .'/../lib/autoload.php';

$app = new \Hello\Application(ENV);
$app->run();