<?php

$configFile = __DIR__ .'/../../../../../config.php';
if(file_exists($configFile)) {
   require_once $configFile;
}
else {
    define('ENV', 'dev');
}

require_once __DIR__ . '/../../../../autoload.php';

