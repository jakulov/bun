<?php
namespace Bun\Core\Http;

/**
 * Interface RequestAwareInterface
 *
 * @package Bun\Core\Http
 */
interface RequestAwareInterface
{
    /**
     * @param Request $request
     *
     * @return void
     */
    public function setRequest(Request $request);
}