<?php
namespace Bun\Core\Http;

use Bun\Core\Config\ApplicationConfig;
use Bun\Core\Config\ConfigAwareInterface;
use Bun\Core\Exception\RoutingException;

/**
 * Class Router
 *
 * @package Bun\Core\Router
 */
class Router implements ConfigAwareInterface, RequestAwareInterface
{
    const DEFAULT_ROUTING_ACTION = 'index';
    /** @var ApplicationConfig */
    protected $config;
    /** @var Request */
    protected $request;
    /** @var array */
    protected $routes = array();

    protected $methods = array(
        'GET',
        'POST',
        'PUT',
        'DELETE'
    );

    /**
     *
     */
    public function __construct()
    {
    }

    /**
     * @param ApplicationConfig $config
     */
    public function setConfig(ApplicationConfig $config)
    {
        $this->config = $config;
    }

    /**
     * @param Request $request
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param bool $ignoreIsConsole
     *
     * @return RoutingResult
     * @throws \Bun\Core\Exception\RoutingException
     */
    public function route($ignoreIsConsole = false)
    {
        if ($this->request->isConsole() && !$ignoreIsConsole) {
            return $this->routeTool();
        }

        $routes = $this->config->get('router');
        $uri = explode('?', $this->request->getUri());
        $uri = $uri[0];
        $routesUris = array_keys($routes);

        $routingResult = new RoutingResult();
        $matchedRoute = in_array($uri, $routesUris) ?
            $matchedRoute = $routes[$uri] :
            $matchedRoute = $this->match($routes);

        if ($matchedRoute) {
            $routingResult
                ->setControllerClass($matchedRoute['controller'])
                ->setActionName(isset($matchedRoute['action']) ?
                        $matchedRoute['action'] :
                        self::DEFAULT_ROUTING_ACTION
                )
                ->setActionParams(isset($matchedRoute['params']) ?
                        $matchedRoute['params'] :
                        array()
                );

            return $routingResult;
        }

        throw new RoutingException(
            'Unable to match route for: ' . $this->request->getMethod() . ': ' . $uri
        );
    }

    /**
     * @return RoutingResult
     * @throws RoutingException
     */
    protected function routeTool()
    {
        // TODO;
    }

    /**
     * @param array $routes
     * @return bool|array
     */
    protected function match($routes)
    {
        $uri = explode('?', $this->request->getUri());
        $uri = $uri[0];
        $method = $this->request->getMethod();
        $routesUri = array_keys($routes);
        $matchedRoute = false;
        foreach ($routesUri as $routeUri) {
            if (strpos($routeUri, ':') !== false) {
                $routeMethods = isset($routes[$routeUri]['method']) ?
                    $routes[$routeUri]['method'] :
                    $this->methods;
                $regexRouteUri = str_replace('/', '\\/', $routeUri);
                $regexRouteUri = '/' . preg_replace('/:(\w+)/', '\w+', $regexRouteUri) . '$/';
                if (preg_match($regexRouteUri, $uri, $matches) && in_array($method, $routeMethods)) {
                    $matchedRoute = $routes[$routeUri];
                    preg_match_all('/:\w+/', $routeUri, $routeParams);
                    preg_match_all('/\w+/', $routeUri, $routeParts);
                    preg_match_all('/\w+/', $uri, $uriParts);
                    $actionParams = array();
                    foreach ($routeParts[0] as $key => $paramName) {
                        if (in_array(':' . $paramName, $routeParams[0])) {
                            $actionParams[$paramName] = isset($uriParts[0][$key]) ?
                                $uriParts[0][$key] :
                                null;
                        }
                    }
                    $matchedRoute['params'] = $actionParams;

                    break;
                }
            }
        }

        return $matchedRoute;
    }
}