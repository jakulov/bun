<?php
namespace Bun\Core\Http;

/**
 * Base interface for different types of controller responses
 *
 * Interface ResponseInterface
 *
 * @package Bun\Core\Http
 */
interface ResponseInterface
{
    /**
     * @return mixed
     */
    public function getContent();

    /**
     * @return array
     */
    public function getHeaders();

    /**
     * @return RunTimer
     */
    public function getTimer();

    /**
     * @param RunTimer $timer
     * @return void
     */
    public function setTimer(RunTimer $timer);
}