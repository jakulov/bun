<?php
namespace Bun\Core\Http;

/**
 * Response allows you to deliver your application content to client browser or terminal (or other client interface)
 * with needed headers. It also allows application to detect how long response was preparing in controller
 *
 * Class Response
 *
 * @package Bun\Core\Http
 */
class Response implements ResponseInterface
{
    /** @var mixed */
    protected $content;
    /** @var array */
    protected $headers = array();
    /** @var RunTimer */
    protected $timer;

    /**
     * @param string $content
     * @param array $headers
     * @param RunTimer $timer
     */
    public function __construct($content = '', $headers = array(), RunTimer $timer = null)
    {
        if ($content !== null) {
            $this->setContent($content);
        }

        if (count($headers) > 0) {
            $this->setHeaders($headers);
        }

        if($timer !== null) {
            $this->setTimer($timer);
        }
    }

    /**
     * @param string $content
     */
    public function setContent($content = '')
    {
        $this->content = $content;
    }

    /**
     * @param $header
     * @param int $code
     */
    public function setHeader($header, $code = 200)
    {
        $this->headers[$code] = $header;
    }

    /**
     * @param array $headers
     * @return $this
     */
    public function setHeaders($headers = array())
    {
        foreach ($headers as $code => $header) {
            $this->setHeader($header, $code);
        }

        return $this;
    }

    /**
     * @param RunTimer $timer
     */
    public function setTimer(RunTimer $timer)
    {
        $this->timer = $timer;
    }

    /**
     * @return mixed`
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @return RunTimer
     */
    public function getTimer()
    {
        return $this->timer;
    }

    /**
     * @return void
     */
    public function sendHeaders()
    {
        foreach ($this->headers as $code => $header) {
            header($header, true, (int)$code);
        }
    }
}