<?php
namespace Bun\Core\Controller;

use Bun\Core\Http\Response;
use Bun\Framework;

/**
 * Class BunController
 * @package Bun\Core\Container
 */
class BunController extends AbstractController
{
    /**
     * @return Response
     */
    protected function indexAction()
    {
        $data = [
            'name' => 'Bun Framework',
            'version' => Framework::VERSION,
        ];

        return new Response($this->render('index', $data));
    }
}