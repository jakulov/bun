<?php
namespace Bun\Core\Controller;

use Bun\Core\Container\Container;
use Bun\Core\Http\ResponseInterface;
use Bun\Core\Application;
use Bun\Core\Http\RoutingResult;

/**
 * Base interface for all controllers
 *
 * Interface ControllerInterface
 *
 * @package Bun\Core\Controller
 */
interface ControllerInterface
{
    const VIEW_DIR = 'View';

    /**
     * @param Application $app
     * @return $this
     */
    public function setApplication(Application $app);

    /**
     * @param RoutingResult $route
     * @return ResponseInterface
     */
    public function run(RoutingResult $route);

    /**
     * @return Container
     */
    public function getContainer();
}