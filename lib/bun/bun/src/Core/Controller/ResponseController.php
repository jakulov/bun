<?php
namespace Bun\Core\Controller;

use Bun\Core\Http\Response;

/**
 * Class ResponseController
 * @package Bun\Core\Controller
 */
class ResponseController extends AbstractController
{
    /**
     * @return Response
     */
    protected function indexAction()
    {
        return new Response('OK. Response time: '. $this->timer->getRunTime() .' sec.');
    }
}