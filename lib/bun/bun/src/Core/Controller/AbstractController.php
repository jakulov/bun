<?php
namespace Bun\Core\Controller;

use Bun\Core\Exception\NotFoundException;
use Bun\Core\Exception\ResponseException;
use Bun\Core\Http\RunTimer;
use Bun\Core\Container\Container;
use Bun\Core\Http\ResponseInterface;
use Bun\Core\Application;
use Bun\Core\Http\RoutingResult;

/**
 * Base Controller class with simple startup logic
 *
 * Class AbstractController
 *
 * @package Bun\Core\Controller
 */
abstract class AbstractController implements ControllerInterface
{
    /** @var Application */
    protected $application;
    /** @var RunTimer */
    protected $timer;
    /** @var RoutingResult */
    protected $route;
    /** @var string */
    protected $defaultAction = 'index';
    /** @var string|null */
    protected $templateEngine;
    /** @var array */
    protected $renderContext = [];

    /**
     * @param Application $app
     * @return $this
     */
    public function setApplication(Application $app)
    {
        $this->application = $app;

        return $this;
    }

    /**
     * Implement your controller init logic here
     */
    protected function init(){}

    /**
     * Implement your controller shutdown logic here
     */
    protected function shutdown(){}

    /**
     * @param RoutingResult $route
     * @return ResponseInterface
     * @throws \Bun\Core\Exception\NotFoundException
     * @throws \Bun\Core\Exception\ResponseException
     */
    public function run(RoutingResult $route)
    {
        $this->timer = new RunTimer(true);
        $this->route = $route;
        $this->init();

        $actionName =
            ($this->route->getActionName() ?
                $this->route->getActionName() :
                $this->defaultAction
            )
            . 'Action';

        if (method_exists($this, $actionName)) {
            /** @var ResponseInterface $response */
            $response = call_user_func_array(
                array($this, $actionName),
                $this->route->getActionParams()
            );
            if(!($response instanceof ResponseInterface)) {
                throw new ResponseException(
                    'Method '. get_class($this) .'::'. $actionName .
                    ' should return ResponseInterface instance'
                );
            }
        }
        else {
            throw new NotFoundException($this->route->getActionNotFoundExceptionMessage(), 404);
        }

        $this->shutdown();
        $response->setTimer($this->timer);

        return $response;
    }

    /**
     * @param $template
     * @param $context
     * @param $engine
     * @return string
     * @throws \Bun\Core\View\ViewException
     */
    protected function render($template, $context, $engine = null)
    {
        if($engine === null && $this->templateEngine !== null) {
            $engine = $this->templateEngine;
        }

        if($this->renderContext) {
            $context = array_replace_recursive($this->renderContext, $context);
        }

        return $this->getViewService()->render($template, $context, $engine);
    }

    /**
     * @return \Bun\Core\View\ViewService
     */
    protected function getViewService()
    {
        return $this->getContainer()->get('bun.core.view.service');
    }

    /**
     * @return Container
     */
    public function getContainer()
    {
        return $this->application->getContainer();
    }
}