<?php
namespace Bun\Core\Tests\View;

use Bun\Core\Container\Container;
use Bun\Core\View\ViewService;
use Bun\Core\Tests\TestApplication;

/**
 * Class TestViewService
 * @package Bun\Core\Tests\View
 */
class ViewServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return TestApplication
     */
    protected function getApp()
    {
        return new TestApplication(ENV);
    }

    public function testAddEngine()
    {
        $viewService = $this->getViewService();
        $engine = new TestViewEngine();
        $viewService->addEngine('test', $engine);

        $this->assertEquals($engine, $viewService->getEngine('test'));
    }

    public function testSetDefaultEngine()
    {
        $viewService = $this->getViewService();
        $engine = new TestViewEngine();
        $viewService->addEngine('test', $engine);
        $viewService->setDefaultEngine('test');

        $this->assertEquals($engine, $viewService->getDefaultEngine());

        $viewService->setDefaultEngine($engine);
        $this->assertEquals($engine, $viewService->getDefaultEngine());

        $this->assertEquals('test', $viewService->getDefaultEngine(true));
    }

    public function testRender()
    {
        $template = 'Hi, %test%';
        $data = array(
            'test' => 'View!',
        );

        $expect = str_replace('%test%', $data['test'], $template);

        $viewService = $this->getViewService();
        $engine = new TestViewEngine();
        $viewService->addEngine('test', $engine);

        $actual = $viewService->render($template, $data, 'test');

        $this->assertEquals($expect, $actual);
    }

    public function testRenderString()
    {
        $template = 'Hi, %test2%';
        $data = array(
            'test2' => 'View2!',
        );

        $expect = str_replace('%test2%', $data['test2'], $template);

        $viewService = $this->getViewService();
        $engine = new TestViewEngine();
        $viewService->addEngine('test', $engine);

        $actual = $viewService->render($template, $data, 'test');

        $this->assertEquals($expect, $actual);
    }

    /**
     * @return ViewService
     */
    protected function getViewService()
    {
        $container = Container::getInstance($this->getApp()->getConfig());

        return $container->get('bun.core.view.service');
    }
}