<?php
namespace Bun\Core\Tests\View;

use Bun\Core\View\AbstractViewEngine;

/**
 * Class TestViewEngine
 * @package Bun\Core\Tests\View
 */
class TestViewEngine extends AbstractViewEngine
{
    /**
     * @param $template
     * @param array $context
     * @return mixed
     */
    public function render($template, $context = [])
    {
        $str = $template;
        foreach($context as $k => $v) {
            $str = str_replace('%'. $k .'%', $v, $str);
        }

        return $str;
    }

    /**
     * @param $string
     * @param array $context
     * @return mixed
     */
    public function renderString($string, $context = [])
    {
        return $this->render($string, $context);
    }
}