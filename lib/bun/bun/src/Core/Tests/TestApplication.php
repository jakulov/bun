<?php
namespace Bun\Core\Tests;

use Bun\Core\Application;

/**
 * Class TestApplication
 *
 * @package Bun\Core\Tests
 */
class TestApplication extends Application
{
    /**
     * @return string
     */
    public function getNameSpace()
    {
        return __NAMESPACE__;
    }
}