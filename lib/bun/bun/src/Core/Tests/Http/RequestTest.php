<?php
namespace Bun\Core\Tests\Http;

use Bun\Core\Http\Request;
use Bun\Core\Tests\TestApplication;

/**
 * Class RequestTest
 *
 * @package Bun\Core\Tests\Http
 */
class RequestTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return TestApplication
     */
    protected function getApp()
    {
        return new TestApplication(ENV);
    }

    /**
     * @return Request
     */
    protected function getRequest()
    {
        return $this->getApp()->getContainer()->get('bun.core.http.request');
    }

    /**
     * Test request getter argv
     */
    public function testGetConsoleArguments()
    {
        global $argv;
        $argv = array('name', 'param');

        $actualArgv = $this->getRequest()->getConsoleArguments();

        $this->assertEquals($argv, $actualArgv);
    }

    public function testIsConsoleRequest()
    {
        $isConsole = true;
        $actualIsConsole = $this->getRequest()->isConsole();

        $this->assertEquals($isConsole, $actualIsConsole);
    }
}