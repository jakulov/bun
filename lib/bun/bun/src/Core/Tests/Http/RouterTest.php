<?php
namespace Bun\Core\Tests\Http;

use Bun\Core\Http\Request;
use Bun\Core\Tests\TestApplication;
use Bun\Core\Http\RoutingResult;

/**
 * Class RouterTest
 *
 * @package Bun\Core\Tests\Http
 */
class RouterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return TestApplication
     */
    protected function getApp()
    {
        return new TestApplication(ENV);
    }

    public function testRoute()
    {
        $_SERVER['REQUEST_URI'] = '/';
        $expectController = 'Bun\\Core\\Controller\\BunController';
        $expectAction = 'index';
        $expectParams = array();

        $request = $this->getApp()->getContainer()->get('http.request');

        /** @var RoutingResult $route */
        $route = $this->getApp()->getContainer()->get('bun.core.http.router')->route(true);

        $this->assertEquals($expectController, $route->getControllerClass());
        $this->assertEquals($expectAction, $route->getActionName());
        $this->assertEquals($expectParams, $route->getActionParams());
    }

    /**
     * Test Router with params
     */
    public function testRouteParams()
    {
        $_SERVER['REQUEST_URI'] = '/bun/docs/Core_Http_Router';
        $_SERVER['HTTP_METHOD'] = 'GET';
        $expectController = 'Bun\\Core\\Controller\\DocsController';
        $expectAction = 'index';
        $expectParams = array('page' => 'Core_Http_Router');

        $request = Request::createFromGlobals();
        /** @var RoutingResult $route */

        /** @var \Bun\Core\Http\Router $router */
        $router = $this->getApp()->getContainer()->get('bun.core.http.router');
        $router->setRequest($request);
        $route = $router->route(true);

        $this->assertEquals($expectController, $route->getControllerClass());
        $this->assertEquals($expectAction, $route->getActionName());
        $this->assertEquals($expectParams, $route->getActionParams());
    }
}