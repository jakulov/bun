<?php
namespace Bun\Core\Tests;

use Bun\Core\Config\ApplicationConfig;

/**
 * Class ApplicationConfigTest
 *
 * @package Bun\Core\Tests
 */
class ApplicationConfigTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Создаем приложение и объект класса настроек
     * @return ApplicationConfig
     */
    protected function getConfig()
    {
        $app = new TestApplication(ENV);
        $config = new ApplicationConfig($app);

        return $config;
    }

    /**
     * Тестим простой доступ к массиву параметров
     */
    public function testGet()
    {
        $expect = array(
            'param1' => 'ok1',
        );

        $actual = $this->getConfig()->get('test1.ns1');
        $this->assertEquals($expect, $actual);
    }

    /**
     * Тестим доступ через dot-нотацию
     */
    public function testGetDot()
    {
        $expect = 'ok2';
        $actual = $this->getConfig()->get('test1.ns2.param2');

        $this->assertEquals($expect, $actual);
    }

    /**
     * Тестим доступ к настройке, определенной в параметрах config.json
     */
    public function testGetParam()
    {
        $expect = 'ok1';
        $actual = $this->getConfig()->get('test1.ns3.params33.parameter1');

        $this->assertEquals($expect, $actual);
    }

    /**
     * Тестим доступ к настройке, определенной в параметрах среды config_dev.json
     */
    public function testGetEnvParam()
    {
        $expect = 'ok2';
        $actual = $this->getConfig()->get('test1.ns3.params33.parameter2');

        $this->assertEquals($expect, $actual);
    }

    /**
     * Тестим доступ к несуществующему параметру
     */
    public function testGetNull()
    {
        $expect = null;
        $actual = $this->getConfig()->get('some.very.unknown.parameter');

        $this->assertEquals($expect, $actual);
    }
}