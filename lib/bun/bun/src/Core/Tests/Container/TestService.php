<?php
namespace Bun\Core\Tests\Container;

/**
 * Class TestService
 *
 * @package Bun\Core\Tests\Container
 */
class TestService implements TestDependencyAwareInterface
{
    /** @var TestDependency */
    public $testDependency;

    /**
     * @param TestDependency $dependency
     */
    public function setTestDependency(TestDependency $dependency)
    {
        $this->testDependency = $dependency;
    }

    public function getName()
    {
        return 'test.service';
    }

}