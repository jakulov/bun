<?php
namespace Bun\Core\Tests\Container;

/**
 * Interface TestDependencyAwareInterface
 *
 * @package Bun\Core\Tests\Container
 */
interface TestDependencyAwareInterface
{
    /**
     * @param TestDependency $dependency
     *
     * @return void
     */
    public function setTestDependency(TestDependency $dependency);
}