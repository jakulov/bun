<?php
namespace Bun\Core\Tests\Container;

/**
 * Class TestDependency
 *
 * @package Bun\Core\Tests\Container
 */
class TestDependency
{
    public $inc = 0;

    public function getName()
    {
        return 'test.dependency';
    }
}