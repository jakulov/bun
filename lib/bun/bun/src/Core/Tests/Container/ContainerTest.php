<?php
namespace Bun\Core\Tests\Container;

use Bun\Core\Container\Container;
use Bun\Core\Tests\TestApplication;

/**
 * Class ContainerTest
 *
 * @package Bun\Core\Tests\Container
 */
class ContainerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return TestApplication
     */
    protected function getApp()
    {
        return new TestApplication(ENV);
    }

    public function testGet()
    {
        $container = Container::getInstance($this->getApp()->getConfig());

        $expect = 'test.service';
        /** @var TestService $testService */
        $testService = $container->get('test.service');

        $this->assertEquals($expect, $testService->getName());

        $expect = 1;
        $testService->testDependency->inc = $expect;

        $testDependency = $container->get('test.dependency');
        $this->assertEquals($expect, $testDependency->inc);
    }
}