<?php
namespace Bun\Core\Container;

use Bun\Core\Config\AbstractConfig;
use Bun\Core\Exception\ContainerException;

/**
 * Class Container
 *
 * @package Bun\Core\Container
 */
class Container
{
    /** @var AbstractConfig */
    protected $config;
    /** @var Container */
    protected static $instance;
    /** @var array */
    protected $serviceConfig = array();
    /** @var array */
    protected $awareConfig = array();
    /** @var array */
    protected $instances = array();
    /** @var array */
    protected $initCallDependencies = array();

    /**
     * @param AbstractConfig $config
     */
    protected function __construct(AbstractConfig $config)
    {
        $this->config = $config;
        $this->instances['bun.core.container'] = $this;
        $this->instances['bun.core.config'] = $config;
        $this->initConfig();
    }

    protected function __clone()
    {

    }

    /**
     * Init configuration of container
     */
    protected function initConfig()
    {
        $this->serviceConfig = $this->config->get('container');
        if(!is_array($this->serviceConfig)) {
            $this->serviceConfig = array();
        }
        if(isset($this->serviceConfig['aware'])) {
            $this->awareConfig = $this->serviceConfig['aware'];
            unset($this->serviceConfig['aware']);
        }
        foreach($this->serviceConfig as $serviceName => $serviceConfig) {
            // init service aliases
            if(isset($serviceConfig['alias'])) {
                if(is_array($serviceConfig['alias'])) {
                    $serviceConfig['alias'][] = $serviceName;
                    foreach($serviceConfig['alias'] as $alias) {
                        $this->serviceConfig[$alias] = $serviceConfig;
                    }
                }
                else {
                    $alias = $serviceConfig['alias'];
                    $serviceConfig['alias'] = array($serviceName, $alias);
                    $this->serviceConfig[$alias] = $serviceConfig;
                }
            }
        }
    }

    /**
     * @param AbstractConfig $config
     *
     * @return Container
     */
    public static function getInstance(AbstractConfig $config)
    {
        if(!self::$instance) {
            self::$instance = new self($config);
        }

        return self::$instance;
    }

    /**
     * @param string $serviceName
     *
     * @return object
     */
    public function get($serviceName)
    {
        if(!isset($this->instances[$serviceName])) {
            $this->initCallDependencies[] = $serviceName;
            $this->instances[$serviceName] = $this->initService($serviceName);
        }
        $this->initCallDependencies = array();

        return $this->instances[$serviceName];
    }

    /**
     * @param string $serviceName
     *
     * @return object
     * @throws \Bun\Core\Exception\ContainerException
     */
    protected function initService($serviceName)
    {
        if(isset($this->serviceConfig[$serviceName])) {
            $serviceConfig = $this->serviceConfig[$serviceName];
            $serviceClass = $serviceConfig['class'];
            $serviceAware = isset($serviceConfig['aware']) ?
                $serviceConfig['aware'] :
                array();

            if(class_exists($serviceClass)) {
                /** @var object $instance */
                $instance = new $serviceClass;
                foreach($this->awareConfig as $interface => $awareConfig) {
                    if($instance instanceof $interface) {
                        foreach($awareConfig as $dependencySetter => $dependencyServiceName) {
                            $serviceAware[$dependencySetter] = $dependencyServiceName;
                        }
                    }
                }

                $this->initServiceDependencies($instance, $serviceAware, $serviceName);

                if(isset($serviceConfig['alias'])) {
                    $this->saveServiceAliases($instance, $serviceConfig['alias']);
                }

                return $instance;
            }

            throw new ContainerException('Service class '. $serviceClass .' does not exists');
        }

        throw new ContainerException('Service '. $serviceName .' does not configured');
    }

    /**
     * @param object $instance
     * @param array $serviceAware
     * @param string $serviceName
     *
     * @throws \Bun\Core\Exception\ContainerException
     */
    protected function initServiceDependencies($instance, $serviceAware = array(), $serviceName)
    {
        foreach($serviceAware as $dependencySetter => $dependencyServiceName) {
            if(strpos($dependencyServiceName, '@') === 0) {
                // init dependency service instance
                $dependencyServiceName = str_replace('@', '', $dependencyServiceName);
                if(!in_array($dependencyServiceName, $this->initCallDependencies)) {
                    $this->initCallDependencies[] = $dependencyServiceName;
                    $dependencyValue = $this->get($dependencyServiceName);
                }
                else {
                    throw new ContainerException('Service '. $serviceName .' has a recursive dependency on '. $dependencyServiceName);
                }
            }
            elseif(strpos($dependencyServiceName, ':') === 0) {
                // dependency is a config parameter
                $dependencyValue = $this->config->get(str_replace(':', '', $dependencyServiceName));
            }
            else {
                // dependency is a constant
                $dependencyValue = $dependencyServiceName;
            }

            if(method_exists($instance, $dependencySetter)) {
                call_user_func_array(array($instance, $dependencySetter), array($dependencyValue));
            }
            else {
                throw new ContainerException('Service '. $serviceName .' should implements dependency setter method: '. $dependencySetter);
            }
        }
    }

    /**
     * @param object $instance
     * @param array $serviceAliases
     */
    protected function saveServiceAliases($instance, $serviceAliases = array())
    {
        if(is_array($serviceAliases)) {
            foreach($serviceAliases as $alias) {
                $this->instances[$alias] = $instance;
            }
        }
    }
}