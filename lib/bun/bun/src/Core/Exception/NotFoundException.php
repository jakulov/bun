<?php
namespace Bun\Core\Exception;

/**
 * Class NotFoundException
 *
 * @package Bun\Core\Exception
 */
class NotFoundException extends BunException
{

} 