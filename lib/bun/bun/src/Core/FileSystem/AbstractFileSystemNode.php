<?php
namespace Bun\Core\FileSystem;

/**
 * Class AbstractFileSystemNode
 *
 * @package Bun\Core\FileSystem
 */
abstract class AbstractFileSystemNode
{
    /** @var string */
    protected $path;
    /** @var string */
    protected $name;
    /** @var string */
    protected $baseDir;

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getBaseDir()
    {
        return $this->baseDir;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}