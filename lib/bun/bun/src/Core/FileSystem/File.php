<?php
namespace Bun\Core\FileSystem;

use Bun\Core\Exception\FileSystemException;

/**
 * Class File
 *
 * @package Bun\Core\FileSystem
 */
class File extends AbstractFileSystemNode
{
    /** @var string|null */
    protected $extension;
    /** @var string */
    protected $content;

    /**
     * @param $path
     * @param bool $create
     * @param int $mode
     */
    public function __construct($path, $create = false, $mode = 0777)
    {
        $this->path = $path;
        $this->baseDir = dirname($path);
        $this->name = basename($path);
        $this->extension = self::getFileExtension($path);

        if($create) {
            $this->create($mode);
        }
    }

    /**
     * @param $mode
     *
     * @throws \Bun\Core\Exception\FileSystemException
     */
    public function create($mode)
    {
        if(!Dir::exists($this->baseDir)) {
            new Dir($this->baseDir, true, $mode);
        }

        if(!self::exists($this->path)) {
            $ok = @touch($this->path);
            if(!$ok) {
                throw new FileSystemException('Unable to touch file: '. $this->path);
            }
            $ok = @chmod($this->path, $mode);
            if(!$ok) {
                throw new FileSystemException('Unable to chmod file: '. $this->path .' in '. $mode);
            }
        }
    }

    /**
     * @param bool $reload
     *
     * @return null|string
     */
    public function getContent($reload = false)
    {
        if($this->content === null || $reload) {
            $this->readContent();
        }

        return $this->content;
    }

    /**
     * @throws \Bun\Core\Exception\FileSystemException
     */
    protected function readContent()
    {
        if(self::isReadable($this->path)) {
            $this->content = @file_get_contents($this->path);
            if($this->content === false) {
                throw new FileSystemException('Unable to read file: '. $this->path);
            }
            return;
        }

        throw new FileSystemException('File: '. $this->path .' is not readable');
    }

    /**
     * @param $path
     *
     * @return bool
     */
    public static function exists($path)
    {
        return is_file($path) && file_exists($path);
    }

    /**
     * @param $path
     *
     * @return bool
     */
    public static function isReadable($path)
    {
        return is_file($path) && is_readable($path);
    }

    /**
     * @param $path
     *
     * @return bool
     */
    public static function isWritable($path)
    {
        return is_file($path) && is_writable($path);
    }

    /**
     * @param $fileName
     *
     * @return string|null
     */
    public static function getFileExtension($fileName)
    {
        $name = basename($fileName);

        return strpos($name, '.') !== false ?
            end(explode('.', $fileName)) :
            null;
    }

    /**
     * @param string $fileName
     *
     * @return string
     */
    public static function getFileNameWithoutExtension($fileName)
    {
        $name = basename($fileName);
        if(strpos($name, '.') !== false) {
            $fileNameParts = explode('.', $fileName);
            $clearedName = array_slice($fileNameParts, 0, count($fileNameParts) - 1);

            return join('.', $clearedName);
        }

        return $fileName;
    }
}