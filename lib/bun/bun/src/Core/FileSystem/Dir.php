<?php
namespace Bun\Core\FileSystem;

use Bun\Core\Exception\FileSystemException;

/**
 * Class Dir
 *
 * @package Bun\Core\FileSystem
 */
class Dir extends AbstractFileSystemNode
{
    /** @var resource */
    protected $context;

    /**
     * @param $path
     * @param bool $create
     * @param int $mode
     */
    public function __construct($path, $create = false, $mode = 0777)
    {
        $this->path = $path;
        $this->baseDir = $path;
        $this->name = basename($path);

        if($create) {
            $this->create($mode, true);
        }
    }

    /**
     * @param int $mode
     * @param bool $recursive
     *
     * @return bool
     * @throws \Bun\Core\Exception\FileSystemException
     */
    public function create($mode = 0777, $recursive = false)
    {
        $ok = @mkdir($this->path, $mode, $recursive);
        if(!$ok) {
            throw new FileSystemException('Unable to create dir: ' . $this->path);
        }

        return $ok;
    }

    /**
     * @return bool
     * @throws \Bun\Core\Exception\FileSystemException
     */
    public function remove()
    {
        if(is_dir($this->path)) {
            $ok = @rmdir($this->path);
            if(!$ok) {
                throw new FileSystemException('Unable to unlink directory: '. $this->path);
            }

            return $ok;
        }

        throw new FileSystemException('Directory: '. $this->path .' not found');
    }

    /**
     * @param $path
     *
     * @return bool
     */
    public static function exists($path)
    {
        return is_dir($path);
    }

    /**
     * @param $path
     *
     * @return bool
     */
    public static function isReadable($path)
    {
        return is_dir($path) && is_readable($path);
    }

    /**
     * @param $path
     *
     * @return bool
     */
    public static function isWritable($path)
    {
        return is_dir($path) && is_writable($path);
    }

    /**
     * @param null $extension
     *
     * @return string
     */
    public function readFile($extension = null)
    {
        $fileName = @readdir($this->getReadDirContext());
        if(!$fileName) {
            return $fileName;
        }

        $filePath = $this->baseDir .'/'. $fileName;
        if(!is_file($filePath)) {
            return $this->readFile($extension);
        }

        if($extension !== null) {
            if(strtolower($extension) === strtolower(File::getFileExtension($fileName))) {
                return $fileName;
            }

            return $this->readFile($extension);
        }

        if($fileName !== '.' && $fileName !== '..') {
            return $fileName;
        }

        return $this->readFile($extension);
    }

    /**
     * @return string
     */
    public function readDir()
    {
        $dirName = @readdir($this->getReadDirContext());
        if(!$dirName) {
            return $dirName;
        }

        if(!is_dir($this->baseDir .'/'. $dirName)) {
            return $this->readDir();
        }

        if($dirName === '.' || $dirName === '..') {
            return $this->readDir();
        }

        return $dirName;
    }

    /**
     * @return $this
     */
    public function clearReadFileContext()
    {
        $this->context = null;

        return $this;
    }

    /**
     * @return resource
     * @throws \Bun\Core\Exception\FileSystemException
     */
    protected function getReadDirContext()
    {
        if($this->context === null) {
            $this->context = @opendir($this->path);
            if(!$this->context) {
                throw new FileSystemException('Unable to read dir: '. $this->path);
            }
        }

        return $this->context;
    }
}