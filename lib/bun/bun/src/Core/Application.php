<?php
namespace Bun\Core;

use Bun\Core\Config\ApplicationConfig;
use Bun\Core\Controller\ControllerInterface;
use Bun\Core\Http\ResponseInterface;
use Bun\Core\Http\RoutingResult;
use Bun\Framework;

/**
 * Class Application
 *
 * @package Bun\Core
 */
abstract class Application
{
    const ENV_DEV = 'dev';
    const ENV_PROD = 'prod';
    const ENV_TEST = 'test';
    /** @var string */
    protected $env;
    /** @var array */
    protected $environments = array(
        self::ENV_DEV,
        self::ENV_PROD,
        self::ENV_TEST,
    );

    /** @var ApplicationConfig */
    protected $config;
    /** @var \Bun\Core\Container\Container */
    protected $container;
    /** @var \Bun\Core\Http\Router */
    protected $router;

    /**
     * @param string $env
     */
    public function __construct($env = self::ENV_DEV)
    {
        $this->env = $env;
        $this->define();
    }

    /**
     * @return array
     */
    public function getEnvironments()
    {
        return $this->environments;
    }

    abstract public function getNameSpace();

    /**
     * @return string
     */
    public function getEnv()
    {
        return $this->env;
    }

    /**
     * Run the application
     */
    public function run()
    {
        $route = $this->getRouter()->route();

        $ok = $this->runController($route);
        if(!$ok) {
            echo('Controller cannot be started');
        }

        return $ok;
    }

    protected function runController(RoutingResult $route)
    {
        // TODO: errors
        $controllerClass = $route->getControllerClass();
        if(class_exists($controllerClass)) {
            $controller = new $controllerClass;
            if($controller instanceof ControllerInterface) {
                $response = $controller->setApplication($this)->run($route);
                if($response && $response instanceof ResponseInterface) {
                    foreach($response->getHeaders() as $code => $header) {
                        if($code >= 200) {
                            header('HTTP 1.1/ ' . $code .' '. $header);
                        }
                        else {
                            header($header);
                        }
                    }
                    echo $response->getContent();

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return ApplicationConfig
     */
    public function getConfig()
    {
        if($this->config === null) {
            $this->config = new ApplicationConfig($this);
        }

        return $this->config;
    }

    /**
     * @return Container\Container
     */
    public function getContainer()
    {
        if($this->container === null) {
            $this->container = Container\Container::getInstance($this->getConfig());
        }

        return $this->container;
    }

    /**
     * @return Http\Router
     */
    public function getRouter()
    {
        if($this->router === null) {
            $this->router = $this->getContainer()->get('bun.core.http.router');
        }

        return $this->router;
    }

    /**
     * Define directory structure
     */
    protected function define()
    {
        $projectRoot = realpath(__DIR__ .'/.././../../../..');
        if(!defined('LIB_DIR')) {
            define('LIB_DIR', $projectRoot . '/lib');
        }
        if(!defined('SRC_DIR')) {
            define('SRC_DIR', $projectRoot . '/src');
        }
        if(!defined('VAR_DIR')) {
            define('VAR_DIR', $projectRoot . '/var');
        }
        if(!defined('PUBLIC_DIR')) {
            define('PUBLIC_DIR', $projectRoot . '/app/public');
        }
        if(!defined('PUBLIC_PATH')) {
            define('PUBLIC_PATH', '/public');
        }
        if(!defined('BUN_DIR')) {
            define('BUN_DIR', realpath(__DIR__ .'/..'));
        }
        if(!defined('DS')) {
            define('DS', DIRECTORY_SEPARATOR);
        }
    }
}