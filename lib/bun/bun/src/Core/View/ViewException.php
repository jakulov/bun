<?php
namespace Bun\Core\View;

use Bun\Core\Exception\BunException;

/**
 * Class ViewException
 *
 * @package Bun\Core\View
 */
class ViewException extends BunException
{

}