<?php
namespace Bun\Core\View;

/**
 * Class ViewServiceInterface
 *
 * @package Bun\Core\View
 */
interface ViewServiceInterface
{
    /**
     * @param $template
     * @param array $context
     * @param ViewEngineInterface|string $engine
     * @return string
     */
    public function render($template, $context = array(), $engine = null);

    /**
     * @param $string
     * @param array $context
     * @param ViewEngineInterface|string $engine
     * @return string
     */
    public function renderString($string, $context = array(), $engine = null);

    /**
     * @param ViewEngineInterface|string $engine
     * @return $this
     */
    public function setDefaultEngine($engine);

    /**
     * @param $name
     * @param ViewEngineInterface $engine
     * @return $this
     */
    public function addEngine($name, ViewEngineInterface $engine);

    /**
     * @param ViewEngineInterface|string $name
     * @return ViewEngineInterface
     */
    public function getEngine($name);

    /**
     * @param bool $returnName
     * @return ViewEngineInterface
     */
    public function getDefaultEngine($returnName = false);
}