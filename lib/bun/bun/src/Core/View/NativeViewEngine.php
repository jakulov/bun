<?php
namespace Bun\Core\View;

/**
 * Class NativeViewEngine
 *
 * @package Bun\Core\View
 */
class NativeViewEngine extends AbstractViewEngine
{
    /** @var string */
    protected $defaultExtension = 'phtml';

    const DEFAULT_TMP_DIR = '/tmp';

    /**
     * @param $template
     * @param array $context
     * @return string
     * @throws ViewException
     */
    public function render($template, $context = array())
    {
        $filename = $this->findTemplate($template);
        if($filename !== null) {
            $data = array_replace_recursive($this->context, $context);

            return $this->renderFile($filename, $data);
        }

        throw new ViewException('Unable to find template "'. $template .'" in directories: '. join(', ', $this->dirs));
    }

    /**
     * @param $filename
     * @param array $data
     * @return string
     */
    protected function renderFile($filename, $data = array())
    {
        ob_start();
        extract($data);

        require $filename;

        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }

    /**
     * @param $string
     * @param array $context
     * @return string
     * @throws ViewException
     */
    public function renderString($string, $context = array())
    {
        $tmpDir = $this->getTmpDir();
        $tmpFile = $tmpDir . DIRECTORY_SEPARATOR . mt_rand(100000, 200000) . '_'. md5(microtime()) .'.' . $this->defaultExtension;
        if(is_writable($tmpDir)) {
            file_put_contents($tmpFile, $string);
            $data = array_replace_recursive($this->context, $context);

            $content = $this->renderFile($tmpDir, $data);
            unlink($tmpFile);

            return $content;
        }

        throw new ViewException('Unable to write to tmp dir: '. $tmpDir);
    }

    /**
     * @return string
     * @throws ViewException
     */
    protected function getTmpDir()
    {
        $dir = (isset($this->options['tmp_dir']) ? $this->options['tmp_dir'] : self::DEFAULT_TMP_DIR) . DS . 'bun_native';
        if(!is_dir($dir)) {
            $ok = mkdir($dir, 0777, true);
            if(!$ok) {
                throw new ViewException('Unable to create tmp_dir: "'. $dir .'"');
            }
        }

        return $dir;
    }
}