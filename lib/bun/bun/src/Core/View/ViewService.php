<?php
namespace Bun\Core\View;

use Bun\Core\Config\ApplicationConfig;
use Bun\Core\Config\ConfigAwareInterface;

/**
 * Class ViewService
 *
 * @package Bun\Core\View
 */
class ViewService implements ViewServiceInterface, ConfigAwareInterface
{
    /** @var array */
    protected $engines = array();
    /** @var ViewEngineInterface */
    protected $defaultEngine;
    /** @var ApplicationConfig  */
    protected $config;
    /** @var array  */
    protected $engineOptions = [];
    /** @var array  */
    protected $templateDirs = [];
    /** @var string */
    protected $defaultEngineName;

    /**
     * @param ApplicationConfig $config
     */
    public function setConfig(ApplicationConfig $config)
    {
        $this->config = $config;
        $this->initTemplateDirs();
        $this->initViewEngines();
    }

    /**
     * @throws ViewException
     */
    protected function initViewEngines()
    {
        $engineOptions = $this->config->get('view.engine_options');
        $engines = $this->config->get('view.engines');
        $this->defaultEngineName = $this->config->get('view.default_engine');
        foreach($engines as $name => $class) {
            if(class_exists($class)) {
                $objReflection = new \ReflectionClass($class);
                if($objReflection->implementsInterface(ViewEngineInterface::class)) {
                    $engine = $objReflection->newInstance($this->templateDirs, $engineOptions);
                    unset($objReflection);
                    $this->engines[$name] = $engine;
                    if(!$this->defaultEngineName) {
                        $this->defaultEngineName = $name;
                        $this->defaultEngine = $engine;
                    }
                }
                else {
                    throw new ViewException(
                        'View engine class "'. $class .'" should implement interface: ' . ViewEngineInterface::class
                    );
                }
            }
            else {
                throw new ViewException('View engine class "'. $class .'" does not exists');
            }
        }

        if(!$this->defaultEngine && $this->defaultEngineName) {
            if(isset($this->engines[$this->defaultEngineName])) {
                $this->defaultEngine = $this->engines[$this->defaultEngineName];
            }
            else {
                throw new ViewException(
                    'Unable to find default view engine with name = "'. $this->defaultEngineName .'"'
                );
            }
        }
        elseif(!$this->defaultEngine) {
            throw new ViewException('Unable to init any of configured template engines!');
        }
    }

    /**
     * @throws ViewException
     */
    protected function initTemplateDirs()
    {
        $templateDirs = $this->config->get('view.template_dirs');
        $bunDirs = [];
        foreach($templateDirs as $dir) {
            if(strpos($dir, '/') === 0) {
                if(is_dir($dir) && is_readable($dir)) {
                    $this->templateDirs[] = $dir;
                }
                else {
                    throw new ViewException('Configured template dir "'. $dir .'" does not exists or is not readable');
                }
            }
            else {
                $appDir = SRC_DIR . DS . $this->config->getApplicationNamespace() . DS . $dir;
                $this->templateDirs[] = $appDir;
                $bunDir = realpath(__DIR__ . DS . '..' . DS . $dir);
                if($bunDir && is_dir($bunDir)) {
                    $bunDirs[] = $bunDir;
                }
            }
        }

        $this->templateDirs = array_merge($this->templateDirs, $bunDirs);
    }

    /**
     * @param $template
     * @param array $context
     * @param ViewEngineInterface|string $engine
     * @throws ViewException
     * @return string
     */
    public function render($template, $context = array(), $engine = null)
    {
        $useEngine = $this->getEngine($engine);
        if($useEngine) {
            return $useEngine->render($template, $context);
        }

        throw new ViewException('Unable to find view engine with name = "'. $engine .'"');
    }

    /**
     * @param $string
     * @param array $context
     * @param ViewEngineInterface|string $engine
     * @throws ViewException
     * @return string
     */
    public function renderString($string, $context = array(), $engine = null)
    {
        $useEngine = $this->getEngine($engine);
        if($useEngine) {
            return $useEngine->renderString($string, $context);
        }

        throw new ViewException('Unable to find view engine with name = "'. $engine .'"');
    }

    /**
     * @param null $name
     * @return ViewEngineInterface|null
     */
    public function getEngine($name = null)
    {
        if($name instanceof ViewEngineInterface) {
            return $name;
        }

        if($name === null) {
            return $this->defaultEngine;
        }

        return isset($this->engines[$name]) ? $this->engines[$name] : null;
    }

    /**
     * @param $name
     * @param ViewEngineInterface $engine
     * @return $this
     */
    public function addEngine($name, ViewEngineInterface $engine)
    {
        $this->engines[$name] = $engine;
    }

    /**
     * @param ViewEngineInterface|string $engine
     * @return $this
     * @throws ViewException
     */
    public function setDefaultEngine($engine)
    {
        if($engine instanceof ViewEngineInterface) {
            foreach($this->engines as $name => $eng) {
                if($eng === $engine) {
                    $this->defaultEngine = $engine;
                    return;
                }
            }

            throw new ViewException('Unable to set ' . get_class($engine) . '#' . spl_object_hash($engine) .' as default engine (not found in added engines: '. join(', ', array_keys($this->engines)) .')');
        }
        elseif(isset($this->engines[$engine])) {
            $this->defaultEngine = $this->engines[$engine];
            return;
        }

        throw new ViewException('Unable to set ' . $engine .' as default engine (not found in added engines: '. join(', ', array_keys($this->engines))  .')');
    }

    /**
     * @param bool $returnName
     * @return ViewEngineInterface|null|string
     */
    public function getDefaultEngine($returnName = false)
    {
        if(!$returnName) {
            return $this->defaultEngine;
        }

        foreach($this->engines as $name => $eng) {
            if($eng === $this->defaultEngine) {
                return $name;
            }
        }

        return null;
    }
}