<?php
namespace Bun\Core\View;

/**
 * Interface ViewEngineInterface
 *
 * @package Bun\Core\View
 */
interface ViewEngineInterface
{
    /**
     * @param array $templateDirs
     * @param array $options
     */
    public function __construct($templateDirs = array(), $options = array());

    /**
     * @param $dir
     * @return $this
     */
    public function addTemplateDir($dir);

    /**
     * @param array $options
     * @return $this
     */
    public function setOptions($options = array());

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function setOption($name, $value);

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function addContext($key, $value);

    /**
     * @param array $context
     * @return $this
     */
    public function setContext($context = array());

    /**
     * @param $template
     * @param array $context
     * @return string
     * @throws ViewException
     */
    public function render($template, $context = array());

    /**
     * @param $string
     * @param array $context
     * @return string
     * @throws ViewException
     */
    public function renderString($string, $context = array());

    /**
     * @param null $key
     * @return mixed
     */
    public function getContext($key = null);
}