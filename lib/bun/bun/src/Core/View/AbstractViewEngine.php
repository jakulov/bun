<?php
namespace Bun\Core\View;

/**
 * Class AbstractViewEngine
 *
 * @package Bun\Core\View
 */
abstract class AbstractViewEngine implements ViewEngineInterface
{
    /** @var array */
    protected $context = array();
    /** @var array */
    protected $dirs = array();
    /** @var string */
    protected $defaultExtension = '';
    /** @var array */
    protected $options = array();

    /**
     * @param array $templateDirs
     * @param array $options
     */
    public function __construct($templateDirs = array(), $options = array())
    {
        $this->context = array();
        $this->dirs = $templateDirs;
        $this->options = $this->initDirsInOptions($options);
    }

    /**
     * @param array $options
     * @return array
     */
    protected function initDirsInOptions($options)
    {
        $dirsOptions = [
            'tmp_dir',
            'cache_dir',
        ];

        foreach($dirsOptions as $do) {
            if(isset($options[$do]) && $options[$do]) {
                if(strpos($options[$do], '/') !== 0) {
                    $options[$do] = VAR_DIR . DS . $options[$do];
                }
            }
        }

        return $options;
    }

    /**
     * @param array $options
     * @return $this
     */
    public function setOptions($options = array())
    {
        $this->options = is_array($options) ? $options : array();

        return $this;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function setOption($name, $value)
    {
        $this->options[$name] = $value;

        return $this;
    }

    /**
     * @param $dir
     * @return $this
     */
    public function addTemplateDir($dir)
    {
        $this->dirs[] = $dir;
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function addContext($key, $value)
    {
        $this->context[$key] = $value;

        return $this;
    }

    /**
     * @param array $context
     * @return $this
     */
    public function setContext($context = array())
    {
        if(is_array($context)) {
            $this->context = $context;
        }
        else {
            $this->context = array();
        }

        return $this;
    }

    /**
     * @param null $key
     * @return array|mixed|null
     */
    public function getContext($key = null)
    {
        if($key !== null) {
            return isset($this->context[$key]) ? $this->context[$key] : null;
        }

        return $this->context;
    }

    /**
     * @param $template
     * @return null|string
     */
    protected function findTemplate($template)
    {
        foreach($this->dirs as $dir) {
            $f = $dir . DIRECTORY_SEPARATOR . $template;
            if(file_exists($f)) {
                return $f;
            }
            elseif($this->defaultExtension) {
                $f .= '.'. $this->defaultExtension;
                if(file_exists($f)) {
                    return $f;
                }
            }
        }

        return null;
    }
}