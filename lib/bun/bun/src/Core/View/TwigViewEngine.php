<?php
namespace Bun\Core\View;

/**
 * Class TwigViewEngine
 *
 * @package Bun\Core\View
 */
class TwigViewEngine extends AbstractViewEngine
{
    /** @var string */
    protected $defaultExtension = 'twig';
    /** @var \Twig_Environment */
    protected $twig;

    /**
     * @param $template
     * @param array $context
     * @return string
     * @throws ViewException
     */
    public function render($template, $context = array())
    {
        $filename = $this->findTemplate($template);
        if($filename !== null) {
            $data = array_replace_recursive($this->context, $context);


        }

        throw new ViewException('Unable to find template "'. $template .'" in directories: '. join(', ', $this->dirs));
    }

    /**
     * @param $string
     * @param array $context
     * @return string
     * @throws ViewException
     */
    public function renderString($string, $context = array())
    {

    }

    protected function getTwig()
    {
        if($this->twig === null) {

        }
    }
}