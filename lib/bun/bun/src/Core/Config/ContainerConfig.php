<?php
namespace Bun\Core\Config;

/**
 * Class ContainerConfig
 *
 * @package Bun\Core\Config
 */
class ContainerConfig extends AbstractConfig
{
    protected $name = 'container';

    protected $config = array(
        'aware' => array(
            'Bun\\Core\\Config\\ConfigAwareInterface' => array(
                'setConfig' => '@bun.core.config'
            ),
            'Bun\\Core\\Http\\RequestAwareInterface' => array(
                'setRequest' => '@bun.core.http.request'
            ),
            'Bun\\Core\\Tests\\Container\\TestDependencyAwareInterface' => array(
                'setTestDependency' => '@test.dependency'
            ),
        ),
        'bun.core.config' => array(
            'class' => 'Bun\\Core\\Config\\ApplicationConfig',
            'alias' => 'config',
        ),
        'bun.core.container' => array(
            'class' => 'Bun\\Core\\Container\\Container',
            'alias' => 'container',
        ),
        'bun.core.http.request' => array(
            'class' => 'Bun\\Core\\Http\\Request',
            'alias' => 'http.request',
        ),
        'bun.core.http.router' => array(
            'class' => 'Bun\\Core\\Http\\Router',
        ),
        'bun.core.view.service' => [
            'class' => 'Bun\\Core\\View\\ViewService',
        ],

        /** test dependency */
        'test.service' => array(
            'class' => 'Bun\\Core\\Tests\\Container\\TestService',
        ),
        'test.dependency' => array(
            'class' => 'Bun\\Core\\Tests\\Container\\TestDependency',
        ),
    );
}