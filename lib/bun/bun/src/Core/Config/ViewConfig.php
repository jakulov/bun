<?php
namespace Bun\Core\Config;

/**
 * Class ViewConfig
 *
 * @package Bun\Core\Config
 */
class ViewConfig extends AbstractConfig
{
    protected $name = 'view';

    protected $config = [
        'engines' => [
            'native' => 'Bun\\Core\\View\\NativeViewEngine'
        ],
        'default_engine' => 'native',
        'template_dirs' => ['Layout'],
        'engine_options' => [
            'tmp_dir' => 'tmp/view',
            'cache_dir' => 'cache/view',
        ],
    ];
}