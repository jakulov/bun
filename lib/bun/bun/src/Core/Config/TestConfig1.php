<?php
namespace Bun\Core\Config;

/**
 * Class TestConfig1
 *
 * @package Bun\Core\Config
 */
class TestConfig1 extends AbstractConfig
{
    protected $name = 'test1';

    protected $config = array(
        'ns1' => array(
            'param1' => 'ok1',
        ),
        'ns2' => array(
            'param2' => 'ok2',
        ),
    );
}