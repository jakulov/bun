<?php
namespace Bun\Core\Config;

/**
 * Class RouterConfig
 *
 * @package Bun\Core\Config
 */
class RouterConfig extends AbstractConfig
{
    protected $name = 'router';

    protected $config = array(
        '/' => array(
            'controller' => 'Bun\\Core\\Controller\\BunController'
        ),
        '/bun/response' => array(
            'controller' => 'Bun\\Core\\Controller\\ResponseController'
        ),
        'bun/docs/:page' => array(
            'controller' => 'Bun\\Core\\Controller\\DocsController',
        ),
    );
}