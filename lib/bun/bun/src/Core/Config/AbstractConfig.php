<?php
namespace Bun\Core\Config;

/**
 * Class AbstractConfig
 *
 * @package Bun\Core\Config
 */
abstract class AbstractConfig implements ConfigInterface
{
    protected $name = 'abstract';

    protected $config = array();

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $param
     *
     * @return mixed|null
     */
    public function get($param)
    {
        if ($param === null) {
            return $this->config;
        }

        if (strpos($param, '.') !== false) {
            $paramParts = explode('.', $param);

            return $this->recursiveGet($paramParts, $this->config);
        }

        return (isset($this->config[$param])) ?
            $this->config[$param] :
            null;
    }

    /**
     * @param array $paramParts
     * @param array $config
     *
     * @return null|mixed
     */
    protected function recursiveGet($paramParts = array(), $config = array())
    {
        $param = array_shift($paramParts);
        if (isset($config[$param])) {
            return (!$paramParts) ?
                $config[$param] :
                $this->recursiveGet($paramParts, $config[$param]);
        }

        return null;
    }
}
