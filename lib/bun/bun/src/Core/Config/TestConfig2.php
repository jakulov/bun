<?php
namespace Bun\Core\Config;

/**
 * Class TestConfig2
 *
 * @package Bun\Core\Config
 */
class TestConfig2 extends AbstractConfig
{
    protected $name = 'test1';

    protected $config = array(
        'ns2' => array(
            'param22' => 'ok22',
        ),
        'ns3' => array(
            'params3' => 'ok3',
            'params33' => array(
                'param333' => 'ok333',
                'parameter1' => ':test.parameter1',
                'parameter2' => ':test.parameter2',
            ),
        ),
    );
}