<?php
namespace Bun\Core\Config;

/**
 *
 * Interface ConfigInterface
 * @package Bun\Core\Config
 */
interface ConfigInterface
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @return array
     */
    public function getConfig();

    /**
     * @param string $param
     * 
     * @return mixed
     */
    public function get($param);
}
