<?php
namespace Bun\Core\Config;

use Bun\Core\Application;
use Bun\Core\Exception\ConfigException;
use Bun\Core\FileSystem\Dir;
use Bun\Core\FileSystem\File;

/**
 * Loads and allows access to all configuration parameters in application
 *
 * Class ApplicationConfig
 *
 * @package Bun\Core\Config
 */
class ApplicationConfig extends AbstractConfig
{
    /** @var Application */
    protected $application;
    /** @var array */
    protected $configParams = array();

    /**
     * @param Application $application
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
        $this->load();
    }

    /**
     * @return mixed
     */
    public function getApplicationNamespace()
    {
        return $this->application->getNameSpace();
    }

    /**
     * loads application config
     */
    protected function load()
    {
        $this->loadBunConfig();
        $this->loadApplicationsConfig();
        $this->loadConfigParams();
    }

    /**
     * Loads config classes from bun modules
     */
    protected function loadBunConfig()
    {
        $bunDir = new Dir(BUN_DIR);
        while($moduleDir = $bunDir->readDir()) {
            $this->loadModuleConfig($bunDir->getBaseDir() .'/'. $moduleDir, 'Bun\\' . $moduleDir);
        }
    }

    /**
     * Loading all applications config and current application config in the end
     */
    protected function loadApplicationsConfig()
    {
        $srcDir = new Dir(SRC_DIR);
        $currentApplicationDir = '';
        while($moduleDir = $srcDir->readDir()) {
            if($moduleDir !== $this->application->getNameSpace()){
                $this->loadModuleConfig($srcDir->getBaseDir() .'/'. $moduleDir, $moduleDir);
            }
            else {
                $currentApplicationDir = $moduleDir;
            }
        }

        $this->loadModuleConfig($srcDir->getBaseDir() .'/'. $currentApplicationDir, $currentApplicationDir);
    }

    /**
     * @throws \Bun\Core\Exception\ConfigException
     */
    protected function loadConfigParams()
    {
        $reload = array();
        foreach($this->config as $param => $value) {
            if(is_array($value)) {
                $reload[$param] = $this->loadRecursiveParam($value);
            }
            elseif(strpos($value, ':') === 0) {
                $paramKey = str_replace(':', '', $value);
                if(isset($this->configParams[$paramKey])) {
                    $reload[$param] = $this->configParams[$paramKey];
                }
                else {
                    throw new ConfigException('Parameter '. $paramKey .' does not specified in config params');
                }
            }
            else {
                $reload[$param] = $value;
            }
        }

        $this->config = $reload;
    }

    /**
     * @param $config
     *
     * @return array
     * @throws \Bun\Core\Exception\ConfigException
     */
    protected function loadRecursiveParam($config)
    {
        $reload = array();
        foreach($config as $param => $value) {
            if(is_array($value)) {
                $reload[$param] = $this->loadRecursiveParam($value);
            }
            elseif(strpos($value, ':') === 0) {
                $paramKey = str_replace(':', '', $value);
                if(isset($this->configParams[$paramKey])) {
                    $reload[$param] = $this->configParams[$paramKey];
                }
                else {
                    throw new ConfigException('Parameter '. $paramKey .' does not specified in config params');
                }
            }
            else {
                $reload[$param] = $value;
            }
        }

        return $reload;
    }

    /**
     * @param $modulePath
     * @param $moduleNameSpace
     *
     * @throws \Bun\Core\Exception\ConfigException
     */
    protected function loadModuleConfig($modulePath, $moduleNameSpace)
    {
        $moduleConfigPath = $modulePath .'/Config';
        if(Dir::exists($moduleConfigPath)) {
            // loading config classes
            $configDir = new Dir($moduleConfigPath);
            while($configFile = $configDir->readFile('php')) {
                $baseClassName = File::getFileNameWithoutExtension($configFile);
                $configClass = $moduleNameSpace .'\\Config\\' . $baseClassName;
                if(
                    $baseClassName !== 'AbstractConfig' &&
                    $baseClassName !== 'ApplicationConfig' &&
                    class_exists($configClass)
                ) {
                    /** @var AbstractConfig $configObj */
                    $configObj = new $configClass();
                    if($configObj instanceof AbstractConfig) {
                        $loaded = isset($this->config[$configObj->getName()]) ?
                            $this->config[$configObj->getName()] :
                            array();
                        $merged = array_replace_recursive($loaded, $configObj->getConfig());
                        $this->config[$configObj->getName()] = $merged;
                    }
                    else {
                        throw new ConfigException('Class '. $configClass .' not a config instance');
                    }
                }
            }
            // loading params from json files
            $configDir->clearReadFileContext();
            $paramsFile = new File($configDir->getBaseDir() .'/config.json');
            if(File::exists($paramsFile->getPath())) {
                $this->loadConfigParamsFile($paramsFile);
            }
            $paramsFile = new File($configDir->getBaseDir() .'/config_'. $this->application->getEnv() .'.json');
            if(File::exists($paramsFile->getPath())) {
                $this->loadConfigParamsFile($paramsFile);
            }
        }
    }

    /**
     * @param File $paramsFile
     *
     * @throws \Bun\Core\Exception\ConfigException
     */
    protected function loadConfigParamsFile(File $paramsFile)
    {
        $configParams = json_decode($paramsFile->getContent(), true);
        if($configParams !== null && is_array($configParams)) {
            $this->configParams = array_replace_recursive($this->configParams, $configParams);
        }
        else {
            throw new ConfigException('Config parameters file: '. $paramsFile->getPath() .
                ' does not contain valid json encoded parameters');
        }
    }
}