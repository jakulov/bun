<?php
namespace Bun\Core\Config;

/**
 * Interface ConfigAwareInterface
 *
 * @package Bun\Core\Config
 */
interface ConfigAwareInterface
{
    /**
     * @param ApplicationConfig $config
     *
     * @return void
     */
    public function setConfig(ApplicationConfig $config);
}