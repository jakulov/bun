<?php
namespace Hello;

/**
 * Class Application
 *
 * @package Hello
 */
class Application extends \Bun\Core\Application
{
    /**
     * @return string
     */
    public function getNameSpace()
    {
        return __NAMESPACE__;
    }
}